﻿using Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace DAO
{
    public interface IBaseDAO<T> where T : class
    {
        bool Add(T data);

        bool Delete(T data);

        bool Update(T data);

        IEnumerable<T> Select(Expression<Func<T, bool>> filter);
    }

    public class BaseDAO<T> : IBaseDAO<T> where T : class
    {
        private DbContext _db;

        public BaseDAO(DbContext db)
        {
            _db = db;
        }

        public bool Add(T data)
        {
            bool added = false;
            
            try
            {
                using (var db = new DbContext("name=DBContext"))
                {
                    var context = db.Set<T>();
                    context.Add(data);
                    int cnt = db.SaveChanges();

                    if (cnt > 0)
                        added = true;
                }

            }
            catch (Exception e)
            {
            }

            return added;
        }

        public bool Delete(T data)
        {
            bool deleted = false;

            try
            {
                using (var db = new DbContext("name=DBContext"))
                {
                    var context = db.Set<T>();
                    context.Remove(data);
                    int cnt = db.SaveChanges();

                    if (cnt > 0)
                        deleted = true;
                }
            }
            catch (Exception e)
            {
            }

            return deleted;
        }

        public IEnumerable<T> Select(Expression<Func<T, bool>> filter)
        {
            IEnumerable<T> result = new List<T>();

            try
            {
                using (var db = new DbContext("name=DBContext"))
                {
                    var context = db.Set<T>();
                    result = context.Where<T>(filter).ToList();
                }
            }
            catch (Exception e)
            {
            }

            if (result == null)
                result = new List<T>();

            return result;
        }

        public bool Update(T data)
        {
            bool added = false;

            try
            {
                using (var db = new DbContext("name=DBContext"))
                {
                    db.Entry(data);
                    int cnt = db.SaveChanges();

                    if (cnt > 0)
                        added = true;
                }
            }
            catch (Exception e)
            {
            }

            return added;
        }
    }
}

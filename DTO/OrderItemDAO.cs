﻿using Domain.BaseClasses;
using Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;

namespace DAO
{
    public interface IOrderItemDAO : IBaseDAO<OrderItem>
    {

    }

    public class OrderItemDAO : IOrderItemDAO
    {
        private IBaseDAO<OrderItem> _crud_operator;

        public OrderItemDAO(IBaseDAO<OrderItem> crud_operator)
        {
            _crud_operator = crud_operator;
        }

        public bool Add(OrderItem data)
        {
            return _crud_operator.Add(data);
        }

        public bool Delete(OrderItem data)
        {
            return _crud_operator.Delete(data);
        }

        public IEnumerable<OrderItem> Select(Expression<Func<OrderItem, bool>> filter)
        {
            return _crud_operator.Select(filter);
        }

        public bool Update(OrderItem data)
        {
            return _crud_operator.Update(data);
        }
    }
}
